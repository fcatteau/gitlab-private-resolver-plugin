# Gitlab Private Package Resolver Plugin #

## Goal ##

This plugin is designed to allow SBT based applications to resolve private Maven packages hosted on Gitlab. Gitlab requires that a personal access token be added to the outgoing headers when resolving private packages, this plugin reads a local credentials file and sets the header.

## Features ##

* Use a local credentials file to access private Gitlab packages.
* Will detect the CI-JOB-TOKEN environmental variable issued during CI pipeline jobs and set the header accordingly.

## Installation ##

Add the following to your `project/plugins.sbt` file:

```scala
// autoplugin
addSbtPlugin("com.nimbleus.glresolver" % "gitlab-private-resolver-plugin" % "1.0.1")
```

Create the credentials file at **$HOME/.gitlab/token-credentials** and add your Gitlab private token.

```
token=xxxxxxxxxxx
```

In your `build.sbt` enable the plugin. 

```scala
enablePlugins(GitlabPrivateResolverPlugin)
```
Add the Gitlab private package location to the list of resolvers. For example this will resolve the private test project for this plugin.

```
lazy val root = (project in file("."))
  .settings(
    scalaVersion := "2.12.4",
    version := "0.1",
    resolvers += "Gitlab Private Repo" at "https://gitlab.com/api/v4/projects/14980924/packages/maven",
    libraryDependencies += "com.stewarc330" % "gitlab-private-resolver-plugin-test-repo" % "0.0.1-SNAPSHOT"
  )
```

## Maintainers ##

- Craig Stewart (@stewarc330)

