import com.nimbleus.glresolver
import sbt.Keys.resolvers



lazy val root = (project in file("."))
  .enablePlugins(GitlabPrivateResolverPlugin)
  .settings(
    scalaVersion := "2.12.6",
    version := "0.1",
    resolvers += "Gitlab Private Repo" at "https://gitlab.com/api/v4/projects/14980924/packages/maven",
    libraryDependencies += "com.stewarc330" % "gitlab-private-resolver-plugin-test-repo" % "0.0.1-SNAPSHOT"
  )