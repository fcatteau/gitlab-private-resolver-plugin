package com.nimbleus.glresolver

/**
  * Define the header types used to authenticate against Gitlab.
  */
object GitlabHeaderType extends Enumeration {
  val JobToken      = Value("Job-Token")
  val PrivateToken  = Value("Private-Token")
}
