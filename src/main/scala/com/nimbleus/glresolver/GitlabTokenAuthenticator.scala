package com.nimbleus.glresolver

import okhttp3.{Interceptor, OkHttpClient, Response}

/**
  * Exposed helper methods for building custom HTTP clients.
  */
object GitlabTokenAuthenticator {
  /**
    * Returns a new custom HTTP builder bound to a private token.
    * @param token the private token read from the local credentials file.
    *
    * @return a new builder.
    */
  def buildHttpBuilder(token: String, headerType: GitlabHeaderType.Value): OkHttpClient.Builder = (new OkHttpClient.Builder).addInterceptor(new GitlabTokenAuthenticator(token, headerType))
}

/**
  * This class represents an interceptor that is called for each external private repository request.
  * The private token is added to the outgoing request.
  *
  * @param token the private token read from the local credentials file.
  */
class GitlabTokenAuthenticator(token: String, headerType: GitlabHeaderType.Value) extends Interceptor {
  override def intercept(chain: Interceptor.Chain): Response = chain.proceed(chain.request.newBuilder.header(headerType.toString, token).build)
}
