package com.nimbleus.glresolver

import sbt.Keys._
import sbt._
import sbt.internal.CustomHttp
import scala.util.{Failure, Success, Try}

/**
  * This SBT plugin extension reads the token from the gitlab token credentials file and then
  * enables the custom HTTP client to apply this token as a header in calls to Gitlab maven
  * private repositories.
  *
  * When running in an automated pipeline environment this plugin will first check for a
  * Gitlab job token.
  */
object GitlabPrivateResolverPlugin extends AutoPlugin {
  private final val EMPTY_TOKEN = ""
  private final val TOKEN_KEY = "token"
  private final val JOB_TOKEN_ENV = "CI_JOB_TOKEN"

  // read token from the user's local file system
  private def localToken : String = {
    Try(IO.readLines(new File(Path.userHome.absolutePath + "/.gitlab/token-credentials"))) match {
      case Success(keyValuePairs) =>
        val tokenPattern = "([^=]*)=(.*)".r
        keyValuePairs.head match {
          case tokenPattern(k, v) => if (k.equalsIgnoreCase(TOKEN_KEY)) v else EMPTY_TOKEN
          case _ => EMPTY_TOKEN
        }
      case Failure(f) => EMPTY_TOKEN
    }
  }

  // first look for the job token environmental variable in case this is running in an automated pipeline
  // otherwise check for a local private token
  val (token, headerType) = sys.env.get(JOB_TOKEN_ENV) match {
    case Some(token) => (token, GitlabHeaderType.JobToken)
    case None => (localToken, GitlabHeaderType.PrivateToken)
  }

  // enable HTTP resolver and build the custom HTTP client.
  override lazy val projectSettings = Seq(
    useCoursier := false,
    updateOptions := (ThisBuild / updateOptions).value.withGigahorse(true),
    CustomHttp.okhttpClient := GitlabTokenAuthenticator.buildHttpBuilder(token, headerType).build()
  )

  override def trigger: PluginTrigger = allRequirements
}